''' decoupled: run a function in a different process '''

import multiprocessing
import queue
from typing import (
        Any,
        Callable,
        Generic,
        Mapping,
        Optional,
        Sequence,
        Tuple,
        TypeVar,
        Union,
)
from dataclasses import dataclass

from decorator import decorator
from tblib import Traceback  # type: ignore

from .typedqueue import TypedQueue


ReturnType = TypeVar('ReturnType')


@dataclass
class ResultSuccess(Generic[ReturnType]):
    ''' The function call was successful '''
    return_value: ReturnType


@dataclass
class ResultError:
    ''' The function call raised an Exception '''
    err: Exception
    traceback: Traceback


Result = Union[ResultSuccess[ReturnType], ResultError]
ResultQueue = TypedQueue[Result[ReturnType]]


@decorator
def decoupled(  # pylint: disable=keyword-arg-before-vararg
        # This is the order decorator needs
        func: Callable[..., ReturnType],
        timeout: Optional[float] = None,
        *args: Any,
        **kwargs: Any,
) -> ReturnType:
    ''' run a function in a different process '''
    result_queue, process = run_subprocess(func, args, kwargs, timeout)

    try:
        return determine_result(result_queue, process)
    finally:
        process.terminate()


def run_subprocess(
        func: Callable[..., ReturnType],
        args: Sequence,
        kwargs: Mapping,
        timeout: Optional[float],
) -> Tuple[ResultQueue[ReturnType], multiprocessing.Process]:
    '''
    run func in a separate process and wait for it to terminate
    (one way or another)
    returns a tuple (result_queue, process), where...
    - result_queue is used to communicate return value / exceptions thrown
    - process is the actual process object
    '''
    result_queue = ResultQueue[ReturnType]()

    process = multiprocessing.Process(
        target=proc,
        args=(result_queue, func, args, kwargs),
    )
    process.start()
    process.join(timeout=timeout)

    return result_queue, process


def proc(
        result_queue: ResultQueue[ReturnType],
        func: Callable[..., ReturnType],
        args: Sequence,
        kwargs: Mapping,
) -> None:
    ''' This is what's run in the other process. '''
    try:
        result = ResultSuccess[ReturnType](
            return_value=func(*args, **kwargs),
        )
        result_queue.put(result)
    except Exception as err:  # pylint: disable=broad-except
        result_error = ResultError(
            err=err,
            traceback=Traceback(err.__traceback__).to_dict(),
        )
        result_queue.put(result_error)


def determine_result(
        result_queue: ResultQueue[ReturnType],
        process: multiprocessing.Process,
) -> ReturnType:
    '''
    Find out what happened in the subprocess
    and return/raise accordingly
    '''
    if process.exitcode is None:
        raise ChildTimeoutError

    try:
        retval = result_queue.get_nowait()
        if isinstance(retval, ResultSuccess):
            return retval.return_value
        raise retval.err.with_traceback(
            Traceback.from_dict(retval.traceback).as_traceback())
    except queue.Empty:
        raise ChildCrashedError from None
    raise ValueError('This should not be reachable - but pylint thinks it is.')


class ChildTimeoutError(Exception):
    ''' thrown when the child process takes too long '''


class ChildCrashedError(Exception):
    ''' thrown when the child process crashes '''
