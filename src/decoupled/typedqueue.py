'''
A wrapper around multiprocessing.Queue that supports type information.
Can be removed once support for Python<3.9 / versions without PEP 585
is dropped.
'''

import multiprocessing
from typing import (
    Generic,
    TypeVar
)


ElementT = TypeVar('ElementT')


class TypedQueue(Generic[ElementT]):
    '''
    A wrapper around multiprocessing.Queue. Only the methods needed in
    decoupled.py are implemented.
    '''
    def __init__(self) -> None:
        self.queue: multiprocessing.Queue = multiprocessing.Queue()

    def put(self, item: ElementT) -> None:
        ''' Put item into the queue. '''
        self.queue.put(item)

    def get_nowait(self) -> ElementT:
        '''
        Remove and return the first item from the queue.
        Raises queue.Empty if the queue is empty.
        '''
        return self.queue.get_nowait()
